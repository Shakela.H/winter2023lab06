public class Board{
	private Die dieOne;
	private Die dieTwo;
	private boolean[] tiles;
	
	public Board(){
		this.dieOne = new Die();
		this.dieTwo = new Die();
		this.tiles = new boolean[12];
	}
	
	public boolean playATurn(){
		this.dieOne.roll();
		this.dieTwo.roll();
		
		System.out.println(dieOne + " "+ dieTwo);
		
		int sumOfDice = (this.dieOne.getFaceValue() + this.dieTwo.getFaceValue());
		int matchDieOnetoTile = this.dieOne.getFaceValue();
		int matchDieTwotoTile = this.dieTwo.getFaceValue();
		
		for (int i = 0; i<this.tiles.length; i++){
			if(!this.tiles[sumOfDice - 1]){
				this.tiles[sumOfDice - 1] = true;
				System.out.println("Closing tile equaling to sum of the dice: " + sumOfDice);
				return false;
			}else if(!this.tiles[matchDieOnetoTile-1]){
				tiles[matchDieOnetoTile - 1] = true;
				System.out.println("Closing tile that matches Die 1: " + matchDieOnetoTile);
				return false;
			}else if(!this.tiles[matchDieTwotoTile-1]){
				tiles[matchDieTwotoTile - 1] = true;
				System.out.println("Closing tile that matches Die 2:" + matchDieTwotoTile);
				return false;
			}else{
				System.out.println("All the tiles for these values are already shut");
				return true;
			}
		}
		return false;
	}
	
	public String toString(){
		String tiles = "";
		int position = 1;
		
		for(int i = 0; i<this.tiles.length; i++){
			if(this.tiles[i]==false){
				tiles += position+" ";
				position++;
			}else{
				tiles += "X ";
				position++;
			}
		}
	return "[ "+tiles+"]";
	}
		
	
}