import java.util.Random;

public class Die{
	private int faceValue;
	private Random rolledDie;
	
	public Die(){
		this.faceValue = 1;
		this.rolledDie = new Random();
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public void roll(){
		this.faceValue = this.rolledDie.nextInt(6)+1;
	}
	
	public String toString(){
		return "Value of die: " + this.faceValue;
	}
}