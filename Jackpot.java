import java.util.Scanner;

public class Jackpot{
	public static void main (String [] args){
		Scanner sc = new Scanner(System.in);
		boolean playAgain = false;
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		int winCount = 0;
		
		System.out.println("Welcome to the Jackpot game! Would you like to start a new game? Y/N");
		
		while(!playAgain){
			Board board = new Board();
		
			if(sc.next().equalsIgnoreCase("y")){
				int newWinCount = startGame(board, gameOver, numOfTilesClosed);
				winCount += newWinCount;
				System.out.println(" ");
				System.out.println("Would you like to play again? y/n");
			}else{
				playAgain = true;
				System.out.println("Thanks for playing!");
				System.out.println("total wins: " + winCount);
			}
		}
	}
	
	public static int startGame(Board board, boolean gameOver, int numOfTilesClosed){
		int winCount = 0;
		while(!gameOver){
			System.out.println("");
			System.out.println(board);
			System.out.println("");
			if(board.playATurn()==true){
				gameOver=true;
			}else{
				numOfTilesClosed++;
			}
		}
		if(numOfTilesClosed >= 7){
			System.out.println("Congrats! you've won!");
			winCount++;
		}else{
			System.out.println("Sorry... you lost.");;
		}
		return winCount;
	}
}